# Simple cryptocurrency arbiter

## Exchangelib

Library to query *depth/orderbook* from multiple exchanges

## Maker1

Currency arbiter. Compares depths on 2 exchanges and finds a profitable trade.
Supports test mode when depth values on one exchange are multiplied by test factor to create a profitable trade each time. E.g. to increase real-time quote by
10% use factor=1.1

## How to build

```
> sbt
> project maker1
> compile
> assembly
> exit
```

## How to run

Real-time:
```
java -jar <path>\bitscala\maker1\target\scala-2.11\maker1-assembly-1.0.jar trade --pairs ETH_BTC,LTC_BTC
CTRL-C to stop
```

In test mode:
```
java -jar <path>\bitscala\maker1\target\scala-2.11\maker1-assembly-1.0.jar trade --pairs ETH_BTC,LTC_BTC --test 1.1
CTRL-C to stop
```

Note: do not run under sbt - CTRL-C is not supported in that mode.

## Help

```
maker1 0.1
Usage: maker1 [trade] [options]

  --help
        prints this usage text
Command: trade [options]
Run a simple arbitrage on 2 exchanges
  --exchanges <e1>,<e2>
        Enter 2 comma separated exchanges from the list: poloniex,bitflyer,bleutrade. Default: poloniex,bleutrade
  --pairs <pair>
        Supported pairs: ETH_BTC,LTC_BTC,XRP_BTC.
        Not all pairs are valid on every exchange. Default: ETH_BTC
  --test <factor>
        Test mode to validate depth merge. To shift one of the depth quotes by 10% set factor to 1.1. Default=false
```


