package exchangelib.bitflyer.constants

import exchangelib.exchange.{ ExchangeConstants}
import exchangelib.defs._
import exchangelib.defs.ExchangePairs._
import exchangelib.defs.ExchangeSymbols._


trait BitflyerConstants extends ExchangeConstants{
  import exchangelib.util.Percent._
  def name = "bitflyer"

  val fee = XactFees(make = 0.4.percent, take = 0.4.percent)
  val pairs:Map[Pair,String] = Map(
    ETH_BTC->"ETH_BTC"
  )

  val symbols:Map[Symbol,String] = Map(ETH->"ETH",
    BTC->"BTC"
  )
}

