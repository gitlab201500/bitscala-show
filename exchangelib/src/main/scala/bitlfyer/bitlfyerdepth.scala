package exchangelib.bitflyer.depth

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.bitflyer.exchange.Bitflyer

import exchangelib.util._
import exchangelib.defs._
import spray.client.pipelining._
import spray.json._
import scala.concurrent.{Promise, Future}
import scala.util.{Failure, Success}
import exchangelib.defs.ExchangePairs.Pair

import spray.httpx.SprayJsonSupport._

private case class DepthEntryBitflyer(price:Double,size:Double)
private case class DepthBitflyer(mid_price:Double,bids:Seq[DepthEntryBitflyer],asks:Seq[DepthEntryBitflyer])

private object BitflyerJsonProtocolDepth extends DefaultJsonProtocol {
  implicit val formatDepthEntryBitflyer = jsonFormat2(DepthEntryBitflyer)
  implicit val formatDepthRequest = jsonFormat3(DepthBitflyer)
}

private object BitflyerConverter {
  import exchangelib.defs.Rate._
  import exchangelib.defs.Coin._
  implicit def toDepthPairBid(v:DepthEntryBitflyer):DepthPairBid = DepthPairBid((v.price).rate,(v.size).coin)
  implicit def toDepthPairAsk(v:DepthEntryBitflyer):DepthPairAsk = DepthPairAsk(v.price.rate,(v.size).coin)
  implicit def toDepthPairsAsk(t:Seq[DepthEntryBitflyer]):Seq[DepthPairAsk]=t.map{p=>toDepthPairAsk(p)}
  implicit def toDepthPairsBid(t:Seq[DepthEntryBitflyer]):Seq[DepthPairBid]=t.map{p=>toDepthPairBid(p)}
}
class DepthQuery(pair:Pair,val as:ActorSystem) extends LazyLogging with ExchangeQuery[Depth] with HttpDebug {
  type Entity = exchangelib.defs.Depth
  implicit val system = as
  import system.dispatcher // execution context for futures
  def httpLogger = logger

  private val _pair = Bitflyer.pairs.getOrElse(pair,s"Unsupported pair: $pair")
  val url = s"https://lightning.bitflyer.jp/v1/getboard?product_code=$pair"

  def fetch():Future[Entity]={
    logger.debug("Calling into bitflyer DepthQuery for : "+_pair)
    epochStart = org.joda.time.DateTime.now.getMillis
    val p = Promise[Entity]()

    def responseFuture() = {
      import BitflyerJsonProtocolDepth._
      val pipeline =
        logRequest(showRequest _ ) ~>
          sendReceive ~>
          logResponse(showResponse _) ~>
          unmarshal[DepthBitflyer]
      pipeline {
        logger.debug("GET: "+url)
        Get(url)
      }
    }

    def execRequest:Unit ={
      import BitflyerConverter._
      responseFuture() onComplete {
        case Success(s) =>
          try {
            logger.debug("Returned : " +s)
            if(s.asks.nonEmpty || s.bids.nonEmpty){

              logger.debug(s"Received Bitflyer depth reply: ${s.toString}")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.success(Depth(Bitflyer.pairsInv.getOrElse(_pair,
                s"Invalid pair is returned from rest call: ${_pair}").asInstanceOf[Pair],DepthInfo(s.asks,s.bids)))

            } else{
              logger.error(s"Error querying bitflyer pair : $pair")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.failure(Error(s"Error querying bitflyer pair : $pair"))
            }
          }catch{
            case e:Exception =>
              logger.error(s"Exception: $e")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.failure(e)
          }

        case Failure(error) =>
          logger.error(s"Couldn't get request. Error: $error")
          epochEnd = org.joda.time.DateTime.now.getMillis
          p.failure(error)
      }
    }
    Future{execRequest}
    p.future
  }
}