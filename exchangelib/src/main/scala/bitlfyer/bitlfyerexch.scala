package exchangelib.bitflyer.exchange

import exchangelib.bitflyer.constants.BitflyerConstants
import exchangelib.bitflyer.depth.DepthQuery
import akka.actor.ActorSystem
import exchangelib.defs.ChartPeriod.Period
import exchangelib.exchange.{Exchange}
import exchangelib.defs.ExchangePairs._


trait BitflyerExchangeApi{
  def depthFactory = {(p:Pair, as:ActorSystem)=>{new DepthQuery(p,as)}}
  def chartFactory = {(p:Pair, period:Period,as:ActorSystem)=>throw new Exception("NYI")}
}

case object Bitflyer extends Exchange with BitflyerConstants with BitflyerExchangeApi