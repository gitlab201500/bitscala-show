package exchangelib.bleutrade.constants

import exchangelib.exchange.{ ExchangeConstants}
import exchangelib.defs._
import exchangelib.defs.ExchangePairs._
import exchangelib.defs.ExchangeSymbols._

trait BleutradeConstants extends ExchangeConstants{
  import exchangelib.util.Percent._
  def name = "bleutrade"

  val fee = XactFees(make = 0.15.percent, take = 0.25.percent)
  val pairs:Map[Pair,String] = Map(ETH_BTC->"ETH_BTC",
    LTC_BTC->"LTC_BTC"
  )

  val symbols:Map[Symbol,String] = Map(ETH->"ETH",
    LTC->"LTC",
    BTC->"BTC"
  )
}

