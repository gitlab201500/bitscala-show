package exchangelib.bleutrade.depth

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.bleutrade.exchange.Bleutrade

import exchangelib.util._
import exchangelib.defs._
import spray.client.pipelining._
import spray.json._
import scala.concurrent.{Promise, Future}
import scala.util.{Failure, Success}
import exchangelib.defs.ExchangePairs.Pair

import spray.httpx.SprayJsonSupport._

private case class DepthPairBleutrade(Quantity:String,Rate:String)
private case class DepthResultBleutrade(buy:Seq[DepthPairBleutrade],sell:Seq[DepthPairBleutrade])
private case class DepthReplyBleutrade(success:String,message:String,result:DepthResultBleutrade)

private object BleutradeJsonProtocolDepth extends DefaultJsonProtocol {
  implicit val formatDepthPairBleutrade = jsonFormat2(DepthPairBleutrade)
  implicit val formatDepthResultBleutrade= jsonFormat2(DepthResultBleutrade)
  implicit val formatDepthReplyBleutrade= jsonFormat3(DepthReplyBleutrade)
}

private object BleutradeConverter {
  import exchangelib.defs.Rate._
  import exchangelib.defs.Coin._
  import exchangelib.util.StringToDouble._
  implicit def toDepthPairBid(v:DepthPairBleutrade):DepthPairBid = DepthPairBid(toDouble(v.Rate).rate,toDouble(v.Quantity).coin)
  implicit def toDepthPairAsk(v:DepthPairBleutrade):DepthPairAsk = DepthPairAsk(toDouble(v.Rate).rate,toDouble(v.Quantity).coin)
  implicit def toDepthPairsAsk(t:Seq[DepthPairBleutrade]):Seq[DepthPairAsk]=t.map{p=>toDepthPairAsk(p)}
  implicit def toDepthPairsBid(t:Seq[DepthPairBleutrade]):Seq[DepthPairBid]=t.map{p=>toDepthPairBid(p)}
}
class DepthQuery(pair:Pair,val as:ActorSystem) extends LazyLogging with ExchangeQuery[Depth] with HttpDebug {
  type Entity = exchangelib.defs.Depth
  implicit val system = as
  import system.dispatcher // execution context for futures
  def httpLogger = logger

  private val _pair = Bleutrade.pairs.getOrElse(pair,s"Unsupported pair: $pair")
  val url = s"https://bleutrade.com/api/v2/public/getorderbook?market=$pair&type=ALL"

  def fetch():Future[Entity]={
    logger.debug("Calling into bleutrade DepthQuery for : "+_pair)
    epochStart = org.joda.time.DateTime.now.getMillis
    val p = Promise[Entity]()

    def responseFuture() = {
      import BleutradeJsonProtocolDepth._
      val pipeline =
        logRequest(showRequest _ ) ~>
          sendReceive ~>
          logResponse(showResponse _) ~>
          HttpUtil.mapTextPlainToApplicationJson ~>
          unmarshal[DepthReplyBleutrade]
      pipeline {
        logger.debug("GET: "+url)
        Get(url)
      }
    }

    def execRequest:Unit ={
      import BleutradeConverter._
      responseFuture() onComplete {
        case Success(s) =>
          try {
            logger.debug("Returned : " +s)
            if (s.success == "false") {
              logger.error(s"Failed depth request for bleutrade: ${s.message}")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.failure(new Exception(s.message))
            } else {
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.success(Depth(Bleutrade.pairsInv.getOrElse(_pair,
                s"Invalid pair is returned from rest call: ${_pair}").asInstanceOf[Pair],
                DepthInfo(s.result.sell,s.result.buy)))
            }
          }catch{
            case e:Exception =>
              logger.error(s"Exception: $e")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.failure(e)
          }

        case Failure(error) =>
          logger.error(s"Couldn't get request. Error: $error")
          epochEnd = org.joda.time.DateTime.now.getMillis
          p.failure(error)
      }
    }
    Future{execRequest}
    p.future
  }
}