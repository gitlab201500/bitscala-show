package exchangelib.bleutrade.exchange

import akka.actor.ActorSystem
import exchangelib.bleutrade.constants.BleutradeConstants
import exchangelib.bleutrade.depth.DepthQuery
import exchangelib.defs.ChartPeriod.Period
import exchangelib.exchange.{Exchange}
import exchangelib.defs.ExchangePairs._


trait BleutradeExchangeApi{
  def depthFactory = {(p:Pair, as:ActorSystem)=>{new DepthQuery(p,as)}}
  def chartFactory = {(p:Pair, period:Period, as:ActorSystem)=>{throw new Exception("NYI")}}
}

case object Bleutrade extends Exchange with BleutradeConstants with BleutradeExchangeApi