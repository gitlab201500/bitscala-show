package exchangelib.defs

import exchangelib.defs.ChartPeriod.Period
import exchangelib.defs.ExchangeSymbols.{XRP, LTC, BTC, ETH,Symbol}
import exchangelib.defs.ExchangePairs.{Pair}
import exchangelib.util.Percent
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._


object ExchangeSymbols{
  sealed abstract class Symbol(val name:String)
  case object BTC extends Symbol("BTC")
  case object ETH extends Symbol("ETH")
  case object LTC extends Symbol("LTC")
  case object XRP extends Symbol("XRP")
  val symbols = Seq(BTC,ETH,LTC,XRP)
}

object ExchangePairs{
  sealed abstract class Pair(val sym:Symbol, val base:Symbol){val name=s"${sym.name}_${base.name}"}
  case object ETH_BTC extends Pair(ETH,BTC)
  case object LTC_BTC extends Pair(LTC,BTC)
  case object XRP_BTC extends Pair(XRP,BTC)
  val pairs = Seq(ETH_BTC,LTC_BTC,XRP_BTC)
}

case class Rate(v:Double) extends Ordered[Rate]{
  def compare(that: Rate):Int = if (this.v < that.v) -1 else if (this.v == that.v) 0 else 1
}
object Rate{
  implicit class toRate(v:Double){
    def rate:Rate = Rate(v)
  }
}
case class Coin(val v:Double) extends Ordered[Coin]{
  def compare(that: Coin):Int = if (this.v < that.v) -1 else if (this.v == that.v) 0 else 1
  def -(that: Coin):Coin=Coin(this.v-that.v)
  def +(that: Coin):Coin=Coin(this.v+that.v)
  def *(that: Double):Coin=Coin(this.v*that)
  def /(that: Coin):Coin=Coin(this.v/that.v)
  def toBaseCoin(r:Rate)=BaseCoin(v*r.v)
}
object Coin{
  implicit class toCoin(v:Double){
    def coin:Coin = Coin(v)
  }
}

case class BaseCoin(val v:Double) extends Ordered[BaseCoin]{
  def compare(that: BaseCoin):Int = if (this.v < that.v) -1 else if (this.v == that.v) 0 else 1
  def -(that: BaseCoin):BaseCoin= BaseCoin(this.v-that.v)
  def +(that: BaseCoin):BaseCoin= BaseCoin(this.v+that.v)
  def *(that: Double):BaseCoin= BaseCoin(this.v*that)
  def /(that: BaseCoin):BaseCoin= BaseCoin(this.v/that.v)
  def toCoin(r:Rate):Coin=Coin(v/r.v)
}
object BaseCoin{
  implicit class toBaseCoin(v:Double){
    def coin:BaseCoin = BaseCoin(v)
  }
}

case class DepthPairBid(q:Rate,v:Coin)
case class DepthPairAsk(q:Rate,v:Coin)
case class DepthInfo(asks:Seq[DepthPairAsk],bids:Seq[DepthPairBid])
case class Depth(pair:Pair,info:DepthInfo)

object ChartPeriod{
  sealed abstract class Period(val name:String, val duration:FiniteDuration)
  case object P5Min extends Period("5 min",300.seconds)
  case object P15Min extends Period("15 min",900.seconds)
  case object P30Min extends Period("30 min",1800.seconds)
  case object P4Hours extends Period("4 hours",14400.seconds)
  case object P24Hours extends Period("24 hours",86400.seconds)
  val periods = Seq(P5Min,P15Min,P30Min,P4Hours,P24Hours)
}

case class ChartInfo(date:Int,high:Rate,low:Rate,open:Rate,close:Rate,volume:Coin)
case class Chart(pair:Pair,period:Period,infos:Seq[ChartInfo])

case class XactFees(make:Percent,take:Percent)

trait ExchangeQuery[T]{
  var epochStart:Long = org.joda.time.DateTime.now.getMillis
  var epochEnd:Long = epochStart
  def rtTime = epochEnd-epochStart
  def fetch():Future[T]
}