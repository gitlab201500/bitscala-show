package exchangelib.exchange

import akka.actor.ActorSystem
import exchangelib.defs.ChartPeriod.Period
import exchangelib.defs._
import exchangelib.defs.ExchangePairs._
import exchangelib.defs.ExchangeSymbols._

trait ExchangeConstants{
  def name:String
  def pairs : Map[Pair,String]
  def pairsInv: Map[String,Pair] = pairs.map(_.swap)
  def symbols : Map[Symbol,String]
  def symbolsInv: Map[String,Symbol] = symbols.map(_.swap)
  def fee :XactFees
}

trait ExchangeApi{
  def depthFactory : ((Pair,ActorSystem)=>ExchangeQuery[Depth])
  def chartFactory : ((Pair,Period,ActorSystem)=>ExchangeQuery[Chart])
}

abstract class Exchange extends ExchangeConstants with ExchangeApi

