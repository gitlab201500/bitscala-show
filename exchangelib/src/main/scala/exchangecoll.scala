package exchangelib

import exchangelib.bitflyer.exchange.Bitflyer
import exchangelib.bleutrade.exchange.Bleutrade
import exchangelib.exchange.Exchange
import exchangelib.poloniex.exchange.Poloniex

object ExchangeCollection{
  val exchanges:Seq[Exchange]=Seq(Poloniex,Bitflyer,Bleutrade)
  val exchangeMap:Map[String,Exchange]=exchanges.map{e=>(e.name,e)}.toMap
}
