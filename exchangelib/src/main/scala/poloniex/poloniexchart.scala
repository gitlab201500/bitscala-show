package exchangelib.poloniex.chart

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.defs.ChartPeriod.Period
import exchangelib.poloniex.exchange.Poloniex
import exchangelib.util._
import exchangelib.defs._
import spray.client.pipelining._
import spray.json._
import scala.concurrent.{Promise, Future}
import scala.util.{Try, Failure, Success}
import exchangelib.defs.ExchangePairs.Pair

import spray.httpx.SprayJsonSupport._


private case class ChartInfoPoloniex(date:Int,high:Double,low:Double,open:Double,close:Double,volume:Double,quoteVolume:Double,weightedAverage:Double)
private case class ChartReplyPoloniex(reply:Either[exchangelib.util.Error,Seq[ChartInfoPoloniex]])
private case class ChartPoloniex(pair:String,period:Period,infos:Seq[ChartInfoPoloniex])


object PoloniexConverter{
  import exchangelib.defs.Rate._
  import exchangelib.defs.Coin._
  implicit def toChartInfo(ci:ChartInfoPoloniex)={
    ChartInfo(ci.date,ci.high.rate,ci.low.rate,ci.open.rate,ci.close.rate,ci.volume.coin)
  }
  implicit def toChart(c:ChartPoloniex):Chart={
    Chart(Poloniex.pairsInv.getOrElse(c.pair,s"Invalid pair is returned from rest call: ${c.pair}").asInstanceOf[Pair],
      c.period,
      c.infos.map{p=>toChartInfo(p)})
  }
}
private object PoloniexJsonProtocolChart extends DefaultJsonProtocol {
  implicit val formatError= jsonFormat1(Error)
  implicit val formatChartInfo= jsonFormat8(ChartInfoPoloniex)
  implicit object formatDepthReplyPoloniex extends RootJsonFormat[ChartReplyPoloniex] {
    def write(c: ChartReplyPoloniex) = throw new Exception ("Method is not supposed to be called")
    def read(value: JsValue) ={
      ChartReplyPoloniex {
        Try {
          value.convertTo[exchangelib.util.Error]
        } match {
          case Success(v) => Left(v)
          case _ =>
            Try {
              value.convertTo[Seq[ChartInfoPoloniex]]
            } match {
              case Success(v) => Right(v)
              case _ =>
                deserializationError("Failed to deserialize ChartReplyPoloniex")
            }
        }
      }
    }
  }
}

import PoloniexJsonProtocolChart._

class ChartQuery(pair:Pair,period:Period,val as:ActorSystem) extends LazyLogging with ExchangeQuery[Chart] with HttpDebug {
  type Entity = exchangelib.defs.Chart
  implicit val system = as
  import system.dispatcher // execution context for futures
  def httpLogger = logger

  private val _pair = Poloniex.pairs.getOrElse(pair,s"Unsupported pair: $pair")
  val url = s"https://poloniex.com/public?command=returnChartData&currencyPair=${_pair}&start=0&end=9999999999&period=${period.duration.toSeconds}"

  def fetch():Future[Entity]={
    logger.debug("Calling into poloniex DepthQuery for : "+_pair)
    epochStart = org.joda.time.DateTime.now.getMillis
    val p = Promise[Entity]()

    def responseFuture() = {
      import PoloniexJsonProtocolChart._
      val pipeline =
        logRequest(showRequest _ ) ~>
          sendReceive ~>
          logResponse(showResponse _) ~>
          unmarshal[ChartReplyPoloniex]
      pipeline {
        logger.debug("GET: "+url)
        Get(url)
      }
    }

    def execRequest:Unit ={
      import PoloniexConverter._
      responseFuture() onComplete {
        case Success(s) =>
          try {
            s.reply match {
              case Left(e)=>
                logger.error(s"Failed chart request for Poloniex: ${e.error}")
                epochEnd = org.joda.time.DateTime.now.getMillis
                p.failure(e)
              case Right(v)=>
                logger.debug(s"Received Poloniex chart reply: ${v.toString}")
                epochEnd = org.joda.time.DateTime.now.getMillis
                p.success(ChartPoloniex(_pair,period,v))
            }
          }catch{
            case e:Exception =>
              logger.error(s"Exception: $e")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.failure(e)
          }



        case Failure(error) =>
          logger.error(s"Couldn't get request. Error: $error")
          epochEnd = org.joda.time.DateTime.now.getMillis
          p.failure(error)
      }
    }
    Future{execRequest}
    p.future
  }
}