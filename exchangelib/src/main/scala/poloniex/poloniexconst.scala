package exchangelib.poloniex.constants

import exchangelib.exchange.{ ExchangeConstants}
import exchangelib.defs._
import exchangelib.defs.ExchangePairs._
import exchangelib.defs.ExchangeSymbols._

trait PoloniexConstants extends ExchangeConstants{
  import exchangelib.util.Percent._
  def name = "poloniex"

  val fee = XactFees(make = 0.15.percent, take = 0.25.percent)
  val pairs:Map[Pair,String] = Map(ETH_BTC->"BTC_ETH",
    LTC_BTC->"BTC_LTC",
    XRP_BTC->"BTC_XRP"
  )

  val symbols:Map[Symbol,String] = Map(ETH->"ETH",
    LTC->"LTC",
    XRP->"XRP",
    BTC->"BTC"
  )
}

