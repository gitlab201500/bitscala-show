package exchangelib.poloniex.depth

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.poloniex.exchange.Poloniex
import exchangelib.util._
import exchangelib.defs._
import spray.client.pipelining._
import spray.json._
import scala.concurrent.{Promise, Future}
import scala.util.{Try, Failure, Success}
import exchangelib.defs.ExchangePairs.Pair

import spray.httpx.SprayJsonSupport._

private case class DepthPairBidPoloniex(q:Double,v:Double)
private case class DepthPairAskPoloniex(q:Double,v:Double)
private case class DepthInfoPoloniex(asks:Seq[DepthPairAskPoloniex],bids:Seq[DepthPairBidPoloniex],isFrozen:String,seq:Int)
private case class DepthPoloniex(pair:String,info:DepthInfoPoloniex)
private case class DepthReplyPoloniex(reply:Either[exchangelib.util.Error,DepthInfoPoloniex])

private object PoloniexConverter{
  import exchangelib.defs.Rate._
  import exchangelib.defs.Coin._
  implicit def toDepthPairBid(v:DepthPairBidPoloniex):DepthPairBid = DepthPairBid((v.q).rate,(v.v).coin)
  implicit def toDepthPairAsk(v:DepthPairAskPoloniex):DepthPairAsk = DepthPairAsk(v.q.rate,(v.v).coin)
  implicit def toDepthPairBids(s:Seq[DepthPairBidPoloniex]):Seq[DepthPairBid] = s.map{p=>toDepthPairBid(p)}
  implicit def toDepthPairAsks(s:Seq[DepthPairAskPoloniex]):Seq[DepthPairAsk] = s.map{p=>toDepthPairAsk(p)}

  implicit def toDepthInfo(v:DepthInfoPoloniex):DepthInfo = DepthInfo(asks=v.asks,bids=v.bids)
  implicit def toDepth(v:DepthPoloniex):Depth = Depth(
  Poloniex.pairsInv.getOrElse(v.pair,s"Invalid pair is returned from rest call: ${v.pair}").asInstanceOf[Pair],v.info)
}

private object PoloniexJsonProtocolDepth extends DefaultJsonProtocol {
  import exchangelib.util.StringToDouble._
  implicit object formatDepthPairAskPoloniex extends RootJsonFormat[DepthPairAskPoloniex] {
    def write(c: DepthPairAskPoloniex) = throw new Exception ("Method is not supposed to be called")
    def read(value: JsValue) = {
      value match {
        case JsArray(a) =>
          if(a.size<2) deserializationError(s"Failed to deserialize DepthPair invalid size: ${a.size}")
          DepthPairAskPoloniex(a(0).convertTo[String],a(1).convertTo[Double])
        case _ =>
          deserializationError("Failed to deserialize DepthPair")
      }
    }
  }
  implicit object formatDepthPairBidPoloniex extends RootJsonFormat[DepthPairBidPoloniex] {
    def write(c: DepthPairBidPoloniex) = throw new Exception ("Method is not supposed to be called")
    def read(value: JsValue) = {
      value match {
        case JsArray(a) =>
          if(a.size<2) deserializationError(s"Failed to deserialize DepthPair invalid size: ${a.size}")
          DepthPairBidPoloniex(a(0).convertTo[String],a(1).convertTo[Double])
        case _ =>
          deserializationError("Failed to deserialize DepthPair")
      }
    }
  }
implicit val formatDepthInfoPoloniex= jsonFormat4(DepthInfoPoloniex)
  implicit val formatDepthError= jsonFormat1(Error)
  implicit object formatDepthReplyPoloniex extends RootJsonFormat[DepthReplyPoloniex] {
    def write(c: DepthReplyPoloniex) = throw new Exception ("Method is not supposed to be called")
    def read(value: JsValue) ={
      DepthReplyPoloniex {
        Try {
          value.convertTo[exchangelib.util.Error]
        } match {
          case Success(v) => Left(v)
          case _ =>
            Try {
              value.convertTo[DepthInfoPoloniex]
            } match {
              case Success(v) => Right(v)
              case _ =>
                deserializationError("Failed to deserialize DepthReplyPoloniex")
            }
        }
      }
    }
  }

}

class DepthQuery(pair:Pair,val as:ActorSystem) extends LazyLogging with ExchangeQuery[Depth] with HttpDebug {
  type Entity = exchangelib.defs.Depth
  implicit val system = as
  import system.dispatcher // execution context for futures
  def httpLogger = logger

  private val _pair = Poloniex.pairs.getOrElse(pair,s"Unsupported pair: $pair")
  val url = s"https://poloniex.com/public?command=returnOrderBook&currencyPair=${_pair}&depth=10"

  def fetch():Future[Entity]={
    logger.debug("Calling into poloniex DepthQuery for : "+_pair)
    epochStart = org.joda.time.DateTime.now.getMillis
    val p = Promise[Entity]()

    def responseFuture() = {
      import PoloniexJsonProtocolDepth._
      val pipeline =
        logRequest(showRequest _ ) ~>
          sendReceive ~>
          logResponse(showResponse _) ~>
          unmarshal[DepthReplyPoloniex]
      pipeline {
        logger.debug("GET: "+url)
        Get(url)
      }
    }

    def execRequest:Unit ={
      import PoloniexConverter._
      responseFuture() onComplete {
        case Success(s) =>
          try {
            s.reply match {
              case Left(e)=>
                logger.error(s"Failed depth request for Poloniex: ${e.error}")
                epochEnd = org.joda.time.DateTime.now.getMillis
                p.failure(e)
              case Right(v)=>
                logger.debug(s"Received Poloniex depth reply: ${v.toString}")
                epochEnd = org.joda.time.DateTime.now.getMillis
                p.success(DepthPoloniex(_pair,v))
            }
          }catch{
            case e:Exception =>
              logger.error(s"Exception: $e")
              epochEnd = org.joda.time.DateTime.now.getMillis
              p.failure(e)
          }



        case Failure(error) =>
          logger.error(s"Couldn't get request. Error: $error")
          epochEnd = org.joda.time.DateTime.now.getMillis
          p.failure(error)
      }
    }
    Future{execRequest}
    p.future
  }
}