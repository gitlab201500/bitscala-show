package exchangelib.poloniex.exchange

import exchangelib.defs.ChartPeriod.Period
import exchangelib.poloniex.constants.PoloniexConstants
import exchangelib.poloniex.depth.DepthQuery
import akka.actor.ActorSystem
import exchangelib.exchange.Exchange
import exchangelib.defs.ExchangePairs._
import exchangelib.poloniex.chart.ChartQuery


trait PoloniexExchangeApi{
  def depthFactory = {(p:Pair, as:ActorSystem)=>{new DepthQuery(p,as)}}
  def chartFactory = {(p:Pair, period:Period, as:ActorSystem)=>{new ChartQuery(p,period,as)}}
}

case object Poloniex extends Exchange with PoloniexConstants with PoloniexExchangeApi