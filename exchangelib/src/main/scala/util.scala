package exchangelib.util

import com.typesafe.scalalogging.Logger
import spray.http._

case class Percent(val pct:Double){
  val value:Double=pct/100.0
}

object Percent{
  implicit class DoubleToPct(v:Double) {
    def percent = Percent(v)
  }
}

object StringToDouble{
  implicit def toDouble(s:String):Double = augmentString(s).toDouble
}

trait HttpDebug{
  def httpLogger:Logger
  def showRequest(request: HttpRequest): Unit = {
    httpLogger.debug(request.toString)}

  def showResponse(reply: HttpResponse): Unit = {
    httpLogger.debug(reply.toString)}
}

case class Error(error:String) extends Exception(error)
object HttpUtil{
  def mapTextPlainToApplicationJson: HttpResponse => HttpResponse = {
    case r@ HttpResponse(_, entity, _, _) =>
      r.withEntity(HttpEntity(ContentType(MediaTypes.`application/json`), entity.data))
    case x => x
  }
}


