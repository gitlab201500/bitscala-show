import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.defs.ExchangePairs
import exchangelib.bitflyer.exchange.Bitflyer
import exchangelib.test.util.DepthTest
import org.scalatest._

class BitflyerSpec extends FlatSpec with Matchers with LazyLogging {

  val system = ActorSystem("DepthSpec")

  "DepthSpec" should "be able to query depth for bitflyer and pairs" in {
    val e = Bitflyer
    for(
        p<-ExchangePairs.pairs
        if e.pairs.contains(p)){
      logger.info(s"Test depth for exchange: ${e.name} pair: ${p}")
      new DepthTest(p,e,system).test1()
    }

  }
}