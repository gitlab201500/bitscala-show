import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.bleutrade.exchange.Bleutrade
import exchangelib.defs.ExchangePairs
import exchangelib.test.util.DepthTest
import org.scalatest._

class BleutradeSpec extends FlatSpec with Matchers with LazyLogging {

  val system = ActorSystem("DepthSpec")

  "DepthSpec" should "be able to query depth for bleutrade and pairs" in {
    val e = Bleutrade
    for(
        p<-ExchangePairs.pairs
        if e.pairs.contains(p)){
      logger.info(s"Test depth for exchange: ${e.name} pair: ${p}")
      new DepthTest(p,e,system).test1()
    }

  }
}