package exchangelib.test

import akka.actor.ActorSystem
import exchangelib.defs.ExchangePairs
import exchangelib.test.util.{ChartTest, DepthTest}
import org.scalatest._
import com.typesafe.scalalogging.LazyLogging
import exchangelib.poloniex.exchange.Poloniex

class PoloniexSpec extends FlatSpec with Matchers with LazyLogging {

  val system = ActorSystem("DepthSpec")

  "DepthSpec" should "be able to query depth for poloniex and pairs" in {
    val e = Poloniex
    for(
        p<-ExchangePairs.pairs
        if e.pairs.contains(p)){
      logger.info(s"Test depth for exchange: ${e.name} pair: ${p}")
      new DepthTest(p,e,system).test1()
    }

  }

  "DepthSpec" should "be able to query chart for poloniex and pairs" in {
    val e = Poloniex
    for(
      p<-ExchangePairs.pairs
      if e.pairs.contains(p)){
      logger.info(s"Test depth for exchange: ${e.name} pair: ${p}")
      new ChartTest(p,e,system).test1()
    }

  }
}
