package exchangelib.test.util

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.defs.ChartPeriod.P24Hours
import exchangelib.defs.ExchangePairs._
import exchangelib.exchange.Exchange
import scala.concurrent.duration._

import scala.concurrent.Await
import scala.util.Try


class DepthTest(pair:Pair,exchange:Exchange,as:ActorSystem) extends LazyLogging {
  def test1(): Unit = {
    val q = exchange.depthFactory(pair, as)
    val r = Try(Await.result(q.fetch(), 60.seconds))
    logger.debug(r.toString)
    logger.debug(s"Round-trip time: ${q.rtTime}")
    assert(q.rtTime>0)
    assert(r.isSuccess)
    assert(r.get.pair == pair)
    assert(r.get.info.asks.head.q >= r.get.info.bids.head.q)
  }
}

class ChartTest(pair:Pair,exchange:Exchange,as:ActorSystem) extends LazyLogging {
  def test1(): Unit = {
    val q = exchange.chartFactory(pair, P24Hours, as)
    val r = Try(Await.result(q.fetch(), 60.seconds))
    logger.debug(r.toString)
    logger.debug(s"Round-trip time: ${q.rtTime}")
    assert(q.rtTime>0)
    assert(r.isSuccess)
    assert(r.get.pair == pair)
    val s = r.get.infos.sortBy(-1*_.date)
    assert(s.size>0)
  }
}