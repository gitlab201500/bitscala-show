
package maker1

import akka.actor.Actor
import com.typesafe.scalalogging.LazyLogging
import exchangelib.exchange.Exchange
import maker1.Messages.DepthResponse

case class ArbActorConfig(maxTradeBtc:Double = 0.2, testMode:TestMode=TestMode(): TestMode)
class ArbActor( exchanges:Map[String,Exchange],
                pair:exchangelib.defs.ExchangePairs.Pair,
                arbActorConf:ArbActorConfig=ArbActorConfig()) extends Actor with LazyLogging {

  logger.info(s"ArbActor: exchanges: ${exchanges.keys.mkString(",")} pair: ${pair.name} ArbActorConfig: $arbActorConf")

  var quotes = collection.mutable.Map.empty[String,DepthResponse]
  def receive = {
    case d:DepthResponse =>
      logger.debug(s"Received depth quote: ${d}")
      if(!quotes.contains(d.exchange) || quotes.get(d.exchange).get.epoch < d.epoch) quotes(d.exchange)=d
      // match exchange with every other one
      quotes.toList.filter { case (k, v) => k != d.exchange }.foreach { case (k, v) =>
        val orders = DepthMatcher(v, quotes(d.exchange),arbActorConf.testMode).orders
        if(orders.isDefined) logger.info(s"*** Found trade **** ${orders.get.statsToString}")
      }
    case _=>
  }
}