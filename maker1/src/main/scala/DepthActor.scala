package maker1
import akka.actor.{Actor, ActorRef}
import com.typesafe.scalalogging.LazyLogging
import exchangelib.exchange.Exchange
import maker1.Messages.{DepthMsg, DepthRequest, DepthResponse}

import scala.concurrent.duration._
import scala.util.{Failure, Success}

case class DepthActorConfig(interval:FiniteDuration=200.milliseconds)
class DepthActor( pair:exchangelib.defs.ExchangePairs.Pair,exchanges:Seq[ActorRef],
                 depthActorConf:DepthActorConfig=DepthActorConfig()) extends Actor with LazyLogging {
  if(exchanges.size<2) throw new FatalException(s"There must be at least 2 exchanges for each pair.")
  import context.dispatcher
  logger.info(s"DepthActor: pairs: ${pair.name} DepthActorConfig: $depthActorConf")
  val tick =
    context.system.scheduler.schedule(500 millis, depthActorConf.interval, self, "tick")

  override def postStop() = tick.cancel()

  def receive = {
    case "tick" =>
      for(e<-exchanges) e!DepthRequest(pair)
  }
}


