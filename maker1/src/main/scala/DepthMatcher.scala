package maker1

import com.typesafe.scalalogging.LazyLogging
import exchangelib.ExchangeCollection
import exchangelib.defs.{BaseCoin, _}
import maker1.Messages.DepthResponse

import scala.annotation.tailrec


private case class TradeBalance(currency:Coin,base:BaseCoin,maxBtc:BaseCoin) {

  def isValid:Boolean = {
    currency>=Coin(0.0) && base >= BaseCoin(0.0) && maxBtc >=BaseCoin(0.0)
  }
}
private case class TradeState(b1:Seq[DepthPairBid],a2:Seq[DepthPairAsk],
                      tos:List[TradeOrder], balance:TradeBalance, p_v:Double, volume:Coin, ifStop:Boolean=false){
  // assert that sorted
  def isValid:Boolean= !ifStop &&
    b1.nonEmpty && a2.nonEmpty &&
    b1.head.q > a2.head.q &&
    balance.isValid &&
    b1.head.q.v > 0.0 && a2.head.q.v > 0.0 &&
    b1.head.v.v > 0.0 && a2.head.v.v > 0.0
}

// Check if there is a trade for a given exchange
private case class DepthMatcher(eIn1:DepthResponse,eIn2:DepthResponse, testMode:TestMode=TestMode()) extends LazyLogging{

  val maxTradeBtc = BaseCoin(10.0)
  val profitThreshold = 1.01
  val profitIncThreshold :Double= (profitThreshold - 1) / 2
  val minTradeBtc = BaseCoin(0.005)
  val pair = eIn2.depth.pair.name

  // sort quotes
  val b1 = eIn1.depth.info.bids.sortBy(p => -p.q.v)
  val a1 = eIn1.depth.info.asks.sortBy(p => p.q.v)
  val b2 = eIn2.depth.info.bids.sortBy(p => -p.q.v)
  val a2 = eIn2.depth.info.asks.sortBy(p => p.q.v)
  
  // if in test mode tweak quotes

  val (e11, e22) = if (!testMode.on) {
    (eIn1.depth.copy(info = eIn1.depth.info.copy(asks = a1, bids = b1)),
      eIn2.depth.copy(info = eIn2.depth.info.copy(asks = a2, bids = b2)))
  } else {
    (eIn1.depth.copy(info = eIn1.depth.info.copy(
      asks = a1.map{p=>DepthPairAsk(Rate(p.q.v*testMode.factor),p.v)},
      bids = b1.map{p=>DepthPairBid(Rate(p.q.v*testMode.factor),p.v)})),
      eIn2.depth.copy(info = eIn2.depth.info.copy(asks = a2, bids = b2)))    
  }
  
  // choose left and right exchange, such that eLeft.highestBid > eRight.lowestAsk
  val ((eLeft,exchLeft),(eRight,exchRight)) =
    if(e22.info.asks.isEmpty || (e11.info.bids.head.q > e22.info.asks.head.q)) 
      ((e11,ExchangeCollection.exchangeMap(eIn1.exchange)),(e22,ExchangeCollection.exchangeMap(eIn2.exchange))) 
    else 
      ((e22,ExchangeCollection.exchangeMap(eIn2.exchange)),(e11,ExchangeCollection.exchangeMap(eIn1.exchange)))

    val feeFactor = (1-exchLeft.fee.take.pct/100)*(1-exchRight.fee.take.pct/100)

    // resulting orders if any
    def orders = {
      val now = org.joda.time.DateTime.now.getMillis

      // Subtract 2Xfee from balances
      // left - currency
      // right - base
      val balLeft = Coin(100) // 100 coins
      val balRight = BaseCoin(1.0) // BTC
      val tbalance = TradeBalance(Coin(balLeft.v*(1-2*exchLeft.fee.take.value)),balRight*(1-2*exchRight.fee.take.value),maxTradeBtc)

      val ts = TradeState(eLeft.info.bids, eRight.info.asks, Nil, tbalance, 0.0, Coin(0.0))
      val m = matchDepths(ts)
      val profit = if(m.volume.v>0 ) m.p_v/m.volume.v else 1.0
      val profitBtc= (BaseCoin(1)-m.balance.maxBtc/maxTradeBtc)*(profit-1)

      if(m.tos.nonEmpty && (maxTradeBtc-m.balance.maxBtc) >minTradeBtc) {
        val epochdelta = math.abs(eIn1.epoch-eIn2.epoch)
        // take eIn2.epoch as the latest time
        Some(OrderCollection(pair,exchLeft.name,exchRight.name,m.tos, profit, m.balance.base,profitBtc, m.volume,eIn2.epoch,epochdelta))
      } else {
        if(m.tos.nonEmpty) logger.debug(s"Total trade is too small (BTC):  ${(maxTradeBtc-m.balance.maxBtc)}")
        None
      }
    }

  @tailrec
  private def matchDepths(in:TradeState):TradeState ={
    // exch2.buy then exch1.sell
    if(!in.isValid){
      in
    } else{
      val askq = in.a2.head.q
      val vols = Seq(in.a2.head.v,in.b1.head.v,in.balance.currency,in.balance.base.toCoin(askq))
      val v1 = vols.sorted.head
      if(v1 > Coin(0.0)){
        logger.debug(s"askq: $askq, v1: $v1, vols: $vols")
        val bidbal = in.balance.currency-v1
        val askbal = in.balance.base-v1.toBaseCoin(askq)
        val balance = in.balance.maxBtc-v1.toBaseCoin(askq)
        val newTBal = TradeBalance(bidbal, askbal, balance)
        logger.debug(s"newTBal: $newTBal")
        if(newTBal.isValid) {
          val profitDelta = in.b1.head.q.v / in.a2.head.q.v * feeFactor - 1.0
          logger.debug(s"profitDelta: $profitDelta")
          if (profitDelta >= profitIncThreshold) {
            val p_v = in.p_v + (profitDelta + 1) * v1.v // profit*volume cumulative
            val volume = in.volume + v1 // volume cumulative
            val profit = p_v / volume.v // profit cumulative
            logger.debug(s"p_v: $p_v, volume: $volume, profit: $profit")
            if (profit > profitThreshold) {
              val e1 = exchLeft.name
              val e2 = exchRight.name
              val tos = in.tos :+ TradeOrder(pair, e2, true, in.a2.head.q.v, v1.v) :+ TradeOrder(pair, e1, false, in.b1.head.q.v, v1.v)
              val dpask = if(in.a2.head.v>v1) DepthPairAsk(in.a2.head.q, in.a2.head.v-v1):: in.a2.tail.toList else in.a2.tail
              val dpbid = if(in.b1.head.v>v1) DepthPairBid(in.b1.head.q, in.b1.head.v-v1) :: in.b1.tail.toList else in.b1.tail
              matchDepths(TradeState(dpbid, dpask, tos, newTBal, p_v, volume))
            } else {
              logger.debug(s"Profit is below threshold: $profit, thresh: $profitThreshold,  pair: $pair, exchange: ${exchLeft.name} ${exchRight.name}")
              matchDepths(in.copy(ifStop = true))}
          } else {
            logger.debug(s"Profit increment is too low: $profitDelta, incthresh: $profitIncThreshold, pair: $pair, exchange: ${exchLeft.name} ${exchRight.name}")
            matchDepths(in.copy(ifStop = true))}
        } else {
          logger.warn(s"Insufficient funds to initiate trade: ${newTBal.toString}, pair: $pair, exchange: ${exchLeft.name} ${exchRight.name}")
          matchDepths(in.copy(ifStop = true))
        }
      } else {
        matchDepths(in.copy(ifStop = true))
      }

    }
  }

  }

