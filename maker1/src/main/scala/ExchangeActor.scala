package maker1

import akka.actor.{Actor, ActorRef}
import com.typesafe.scalalogging.LazyLogging
import exchangelib.exchange.Exchange
import maker1.Messages.{DepthRequest, DepthResponse}

import scala.util.{Failure, Success}

class ExchangeActor(exchange:Exchange,arbActors:Map[exchangelib.defs.ExchangePairs.Pair,ActorRef]) extends Actor with LazyLogging {
  import context.dispatcher
  logger.info(s"ExchangeActor: exchange ${exchange.name} pairs: ${arbActors.keys.map{_.name}.mkString(",")}")
  def receive = mainLoop
  def mainLoop:Receive = {
    case DepthRequest(pair) =>
      val q = exchange.depthFactory(pair, this.context.system)
      q.fetch().onComplete{
        case Success(s)=>
          arbActors(s.pair) ! DepthResponse(q.epochEnd,exchange.name,s)
        case Failure(f)=>
          logger.error(s"Failed depth request for exchange ${exchange.name} pair: ${pair}")
      }
    case _=>
  }

}

