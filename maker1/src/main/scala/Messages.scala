package maker1

import exchangelib.defs.Depth
import exchangelib.exchange.Exchange

import scala.util.Try

trait MakerMsg

object Messages {
  case class Start() extends MakerMsg
  case class Stop() extends MakerMsg
  case class ArbStartRequest(exchanges:Seq[Exchange], pairs:Seq[String],testMode:TestMode) extends MakerMsg

  case class DepthRequest(pair:exchangelib.defs.ExchangePairs.Pair) extends MakerMsg
  case class DepthResponse(epoch:Long, exchange:String,depth:Depth) extends MakerMsg
  case class DepthMsg(depth: Depth) extends MakerMsg
}


