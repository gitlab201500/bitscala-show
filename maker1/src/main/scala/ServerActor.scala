package maker1
import akka.actor.SupervisorStrategy.{Escalate, Restart}
import akka.actor._
import akka.util.Timeout
import com.typesafe.scalalogging.LazyLogging
import exchangelib.ExchangeCollection
import maker1.Messages.{ArbStartRequest, Start, Stop}

import scala.concurrent.duration._
import scala.util.{Failure, Try}


class ServerActor() extends Actor with LazyLogging {

  implicit val timeout = Timeout(1.seconds)

  override val supervisorStrategy: SupervisorStrategy = {
    val decider: SupervisorStrategy.Decider = {
      case _: ActorInitializationException => Escalate
      case _: ActorKilledException => Escalate
      case _: FatalException => Escalate
      case _: Exception => Restart
    }
    OneForOneStrategy()(decider orElse super.supervisorStrategy.decider)
  }

  def receive = initial

  def initial: Receive = {
    case Start() =>
      context become initActors
  }

  def initActors: Receive = {
    case a: ArbStartRequest =>
      logger.debug(s"initActors-> waitForInit")
      context become waitForInit
      Try{
        val bb = for(p<-a.pairs;
        e<-a.exchanges;
        s<-e.pairs.keys; if(s.name==p)
        ) yield(s,e)
        val pp = bb.groupBy(_._1).flatMap { p =>
            if (p._2.size > 1)
              Some((p._1, p._2.map(_._2)))
            else {
              logger.warn(s"Pair ${p._1.name} is not supported at least 2 exchanges, so it is dropped.")
              None
            }
          }
        if(pp.size<1) throw new FatalException(s"Empty pair set")

        val arbs = pp.map{p=>(p._1,context.actorOf(Props(new ArbActor(p._2.map{e=>(e.name,e)}.toMap,p._1,
          ArbActorConfig(testMode=a.testMode))),
          s"ExchangeActor-${p._1.name}"))}
        val exchActors = pp.flatMap(_._2).toList.distinct.map{e=>
          (e.name,context.actorOf(Props(new ExchangeActor(e,arbs.filter{a=>
            ExchangeCollection.exchangeMap(e.name).pairs.contains(a._1)})), s"ExchangeActor-${e.name}"))}.toMap
        pp.map{p=>context.actorOf(Props(new DepthActor(p._1,p._2.map{e=>exchActors(e.name)})), s"DepthActor-${p._1}")}
      } match {
        case Failure(f) =>
          logger.error(s"Failed to start Arb: $f")
          context.system.terminate()
        case _ =>
      }

  }

  def waitForInit: Receive = {
    case _ =>
  }

  def waitForStop: Receive = {
    case Stop() =>
      if (context.system != null) {
        context.system.terminate()
      }
  }

  override def postStop() = {
    logger.debug(s"In postStop")
    if (context !=null && context.system != null) {
      context.system.terminate()
    }
  }
}

object  ServerActor {
  def make(system: ActorSystem): ActorRef = {
    system.actorOf(Props(new ServerActor()), "server")
  }
}
