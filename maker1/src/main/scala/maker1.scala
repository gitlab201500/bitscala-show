package maker1

import _root_.akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import exchangelib.ExchangeCollection
import exchangelib.exchange.Exchange
import maker1.Messages.{ArbStartRequest, Start}

import scala.concurrent.Await
import scala.concurrent.duration._

case class Config(
                   mode: Option[Config=>Unit] = None,
                   exchanges:Seq[String]=Seq("poloniex","bleutrade"),
                   pairs:Seq[String]=Seq("ETH_BTC"),
                   testMode:TestMode=TestMode(false,1.1)
                 ){
  val exchanges1 = exchanges.map{e=> ExchangeCollection.exchangeMap.getOrElse(e,throw new Exception(s"Exchange $e is not found"))
  }
}

object Bittrader extends LazyLogging{
  sys addShutdownHook(shutdown)

  private var system: Option[ActorSystem] = None

  def main(args: Array[String]):Unit = {
    val sys = ActorSystem("AkkaClient")
    system = Some(sys)
    val server = ServerActor.make(sys)
    server ! Start()

    val parser = new scopt.OptionParser[Config]("maker1") {
      head("maker1", "0.1")
      help("help") text ("prints this usage text")
      cmd("trade") action { (_, c) =>
        c.copy(mode = Some({c=>
          //if (c.exchanges.size !=2 ) throw new Exception("Only 2 exchanges are supported")
          server ! ArbStartRequest(c.exchanges1, c.pairs, c.testMode
            )})) } text("Run a simple arbitrage on 2 exchanges") children(
        opt[Seq[String]]("exchanges") valueName(s"<e1>,<e2>") text(
          s"Enter 2 comma separated exchanges from the list: ${ExchangeCollection.exchanges.map(_.name).mkString(",")}." +
            s" Default: ${Config().exchanges.mkString(",")}"
          )
          optional() action { (x, c) =>
          c.copy(exchanges = x) },
        opt[Seq[String]]("pairs") valueName(s"<pair>") text(
          s"""Supported pairs: ${ExchangeCollection.exchanges.map{_.pairsInv}.flatten.map{_._2}.distinct.mkString(",")}.
             |        Not all pairs are valid on every exchange. Default: ${Config().pairs.mkString(",")}""".stripMargin
          ) optional() action { (x, c) =>
          c.copy(pairs = x) },
        opt[Double]("test") valueName("<factor>") optional() text(
          s"""Test mode to validate depth merge. To shift one of the depth quotes by 10% set factor to 1.1. Default=${Config().testMode.on}"""
          ) action { (x, c) =>
          c.copy(testMode = TestMode(true,x)) }
        )
    }

    parser.parse(args, Config()) match {
      case Some(config) =>
        if (config.mode.isEmpty) {
          logger.error("Invalid mode")
          parser.usage
          exit("",1)
        } else {
          try {
            (config.mode.get) (config)
          } catch {
            case e: Throwable =>
              logger.error("Exception thrown in  " + config.mode.get.toString + " exception: " + e.toString)
              exit("",1)
          }
        }

      case None =>
      // arguments are bad, error message will have been displayed
        exit("",1)
    }
  }

  private def exit(message: String, status: Int): Nothing = {        // <20>
    for (sys <- system) {
      Await.result(sys.terminate(),60.seconds)
    }
    println(message)
    sys.exit(status)
  }

  private def shutdown() {
    logger.info("Main shutdown ...")
    for (sys <- system) {
      Await.result(sys.terminate(),60.seconds)
    }
  }
}
