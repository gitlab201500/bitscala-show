package maker1

import exchangelib.defs.{BaseCoin, Coin}

class FatalException(msg:String) extends Exception(msg)

case class TradeOrder(pair:String, exchange:String, isBuy:Boolean, price:Double,volume:Double)
case class OrderCollection(pair:String,e1:String,e2:String,orders: Seq[TradeOrder],
                           profit:Double,balanceBtc:BaseCoin,profitBtc:BaseCoin,volume:Coin,epoch:Long,ed:Long){
  def statsToString:String = {
    s"$epoch e1: $e1, e2: $e2, profit: $profit, profitBtc: ${profitBtc.v}, remaining balanceBtc: ${balanceBtc.v}, total trade volume: ${volume.v}, epoch_delta_ms: $ed"
  }
}
case class TestMode(on:Boolean=false,factor:Double=1.0)