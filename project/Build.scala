import sbt._
import Keys._
import sbtassembly.AssemblyKeys._

object Build extends Build {
  lazy val root = (project in file(".")).
    aggregate(exchangelib).
    aggregate(maker1)

  lazy val exchangelib = Project(id = "exchangelib", base = file("exchangelib")).settings(
    name := "exchangelib",
    version := "1.0",
    scalaVersion := "2.11.6",
    libraryDependencies ++= Seq(
      "com.github.scopt" %% "scopt" % "3.3.0",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
      "ch.qos.logback" % "logback-classic" % "1.1.2",
      "io.spray" % "spray-json_2.11" % "1.3.2",
      "io.spray" % "spray-can_2.11" % "1.3.3",
      "io.spray" % "spray-httpx_2.11" % "1.3.3",
      "io.spray" % "spray-client_2.11" % "1.3.3",
      "io.spray" % "spray-util_2.11" % "1.3.3",
      "com.github.nscala-time" % "nscala-time_2.10" % "2.4.0",
      "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
      "org.scala-saddle" % "saddle-core_2.11" % "1.3.4",
      "org.scala-saddle" % "saddle-hdf5_2.11" % "1.3.4",
      "com.typesafe.akka" % "akka-actor_2.11" % "2.4.6",
      "com.typesafe.akka" % "akka-stream_2.11" % "2.4.6",
      "com.typesafe.akka" % "akka-http-core_2.11" % "2.4.6",
      "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % "2.4.6",
      "com.sumologic.elasticsearch" % "elasticsearch-core" % "1.0.16"
    ),
    test in assembly :={},
    resolvers += Resolver.sonatypeRepo("public"),
    fork in run := true
  )
  lazy val maker1 = Project(id = "maker1", base = file("maker1")).settings(
    name := "maker1",
    version := "1.0",
    scalaVersion := "2.11.6",
    libraryDependencies ++= Seq(
      "com.github.scopt" %% "scopt" % "3.3.0",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
      "ch.qos.logback" % "logback-classic" % "1.1.2",
      "io.spray" % "spray-json_2.11" % "1.3.2",
      "io.spray" % "spray-can_2.11" % "1.3.3",
      "io.spray" % "spray-httpx_2.11" % "1.3.3",
      "io.spray" % "spray-client_2.11" % "1.3.3",
      "io.spray" % "spray-util_2.11" % "1.3.3",
      "com.github.nscala-time" % "nscala-time_2.10" % "2.4.0",
      "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
      "org.scala-saddle" % "saddle-core_2.11" % "1.3.4",
      "org.scala-saddle" % "saddle-hdf5_2.11" % "1.3.4",
      "com.typesafe.akka" % "akka-actor_2.11" % "2.4.6",
      "com.typesafe.akka" % "akka-stream_2.11" % "2.4.6",
      "com.typesafe.akka" % "akka-http-core_2.11" % "2.4.6",
      "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % "2.4.6"
    ),
    test in assembly :={},
    parallelExecution in Test := false,
    resolvers += Resolver.sonatypeRepo("public"),
    fork in run := true
  ).dependsOn(exchangelib)


  lazy val assemblySettings = Seq(
    test in assembly := {} //noisy for end-user since the jar is not available and user needs to build the project locally
  )
}